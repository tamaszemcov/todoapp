package gui;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import data.Filter;
import data.Priority;
import data.ToDo;

public class ToDoListModelTest {

	ArrayList<ToDo> list;
	Filter filterValue;
	ToDoListModel model;
	ToDo toDo;
	ToDo toDoForDelete;
	
	
	@Before
	public void setUp() {
	 list = new ArrayList<ToDo>();
	 toDoForDelete = new ToDo("Deleted todo", "description for deleted todo", new Date(), Priority.MEDIUM_PRIORITY, true); 
	 list.add(toDoForDelete);
	 
	 filterValue = Filter.DONE;
	 model = new ToDoListModel(filterValue, list);
	 toDo = new ToDo("Test todo", "description for test todo", new Date(), Priority.MEDIUM_PRIORITY, true);
	} 
	
	@Test
	public void testAdd() {
		model.add(toDo);
		Assert.assertTrue(model.toDoList.contains(toDo)); 
	}

	@Test
	public void testDelete() {
		model.delete(toDoForDelete);
		Assert.assertFalse(model.toDoList.contains(toDoForDelete)); 
	}
	
	@Test
	public void testGetSize() {
		Assert.assertEquals(1 ,model.getSize());
	}
	
	@Test
	public void testUpdate() {
		model.update(Filter.EXPIRED);
		Assert.assertEquals(Filter.EXPIRED ,model.filterValue);
	}
	
	@Test
	public void testGetElementAt() {
		Assert.assertEquals(toDoForDelete ,model.getElementAt(0));
	}
	
}
