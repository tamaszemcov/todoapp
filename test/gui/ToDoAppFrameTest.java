package gui;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import data.Priority;
import data.ToDo;

public class ToDoAppFrameTest {

	ToDoAppFrame frame;
	
	@Before
	public void setUp() {
		frame = new ToDoAppFrame(); 		
	 } 
	
	@Test
	public void testTitle() {
		Assert.assertEquals("ToDoApp", frame.getTitle());
		
	}
	
	@Test
	public void testResizable() {
		Assert.assertFalse(frame.isResizable());
		
	}

}
