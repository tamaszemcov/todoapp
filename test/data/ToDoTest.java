package data;

import static org.junit.Assert.*;
import gui.ToDoListModel;

import java.util.ArrayList;
import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ToDoTest {

	ToDo toDo;
	
	@Before
	public void setUp() {
	 toDo = new ToDo("Test todo", "description for test todo", new Date(), Priority.MEDIUM_PRIORITY, true);
	} 
	
	@Test
	public void testGetDescription() {
		Assert.assertEquals("description for test todo", toDo.getDescription());
	}
	
	@Test
	public void testSetPriority() {
		toDo.setPriority(Priority.LOW_PRIORITY);
		Assert.assertEquals(Priority.LOW_PRIORITY, toDo.getPriority());
	}
	
	@Test
	public void testToString() {		
		Assert.assertEquals(toDo.getName(), toDo.toString());
	}
	

}
