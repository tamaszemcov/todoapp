package data;

import java.io.Serializable;
import java.util.Date;

public class ToDo implements Serializable {

	private String name;
	private String description;
	private Date dueDate;
	private Priority priority;
	private boolean isDone;
	
	public ToDo(String name, String description, Date dueDate,
			Priority priority, boolean isDone) {
		this.name = name;
		this.description = description;
		this.dueDate = dueDate;
		this.priority = priority;
		this.isDone = isDone;
	}
	
	@Override
	public String toString() {
		return name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public Priority getPriority() {
		return priority;
	}

	public void setPriority(Priority priority) {
		this.priority = priority;
	}

	public boolean isDone() {
		return isDone;
	}

	public void setDone(boolean isDone) {
		this.isDone = isDone;
	}
	
	
	
	
}
