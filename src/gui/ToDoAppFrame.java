package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import data.Filter;
import data.ToDo;
import data.Priority;

public class ToDoAppFrame extends JFrame {
	
	private ArrayList<ToDo> toDoList;
	
	private JTextField nameField;
	private JTextArea descriptionArea;
	
	private JSpinner dueDateYearSpinner;
	private JSpinner dueDateMonthSpinner;		
	private JSpinner dueDateDaySpinner;
	
	private JComboBox<Priority> priorityComboBox;
	
	private JCheckBox isDoneCheckBox;
	
	private int ToDoIndex = -1;
	
	private JComboBox<Filter> filterComboBox;
	Filter filterValue;
	
	ToDoListModel model;
	JList list;
	
	public ToDoAppFrame() {
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(1200, 800);
		setResizable(false);
		setTitle("ToDoApp");
		
		addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
                saveToDoData();
                e.getWindow().dispose();
            }
        });
		
		createMenuBar();
		
		createToDoData();
		
		createContent();
		
		
	}
	
	
	
	private void createMenuBar() {
		
		
		JMenuBar menuBar = new JMenuBar();
		JMenu fileMenu = new JMenu("File");		
		JMenu helpMenu = new JMenu("Help");
		
		//File menu
		JMenuItem newTodo = new JMenuItem("New ToDo");
		JMenuItem exit = new JMenuItem("Exit");
		
		newTodo.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				JTextField addNameField;
				JTextArea addSescriptionArea;				
				JSpinner addDueDateYearSpinner;
				JSpinner addDueDateMonthSpinner;		
				JSpinner addDueDateDaySpinner;				
				JComboBox<Priority> addPriorityComboBox;				
				JCheckBox addIsDoneCheckBox;

				addNameField = new JTextField(30);
				addSescriptionArea = new JTextArea(5, 40);				
				addDueDateYearSpinner = new JSpinner();
				addDueDateMonthSpinner = new JSpinner();		
				addDueDateDaySpinner = new JSpinner();				
				addPriorityComboBox = new JComboBox<Priority>();				
				addIsDoneCheckBox = new JCheckBox("Task completed");
				
				Date currentDate = new Date();
				SpinnerModel yearModel = new SpinnerNumberModel(currentDate.getYear()+1900, currentDate.getYear() +1800, currentDate.getYear() + 2000, 1);
				addDueDateYearSpinner.setModel(yearModel);
				SpinnerModel monthModel = new SpinnerNumberModel(currentDate.getMonth()+1, 1, 12, 1);
				addDueDateMonthSpinner.setModel(monthModel);
				SpinnerModel dayModel = new SpinnerNumberModel(currentDate.getDate(), 1, 31, 1);
				addDueDateDaySpinner.setModel(dayModel);				
				addPriorityComboBox.addItem(Priority.LOW_PRIORITY);
				addPriorityComboBox.addItem(Priority.MEDIUM_PRIORITY);
				addPriorityComboBox.addItem(Priority.HIGH_PRIORITY);
				
				JPanel addPanel = new JPanel();
				addPanel.setLayout(new BoxLayout(addPanel, BoxLayout.Y_AXIS));
				
				addPanel.add(addNameField);
				addPanel.add(addSescriptionArea);
				addPanel.add(addDueDateYearSpinner);
				addPanel.add(addDueDateMonthSpinner);
				addPanel.add(addDueDateDaySpinner);
				addPanel.add(addPriorityComboBox);
				addPanel.add(addIsDoneCheckBox);
			      
				
			      int result = JOptionPane.showConfirmDialog(null, addPanel, 
			               "Add new todo", JOptionPane.OK_CANCEL_OPTION);
			      if (result == JOptionPane.OK_OPTION) {
			         ToDo addedToDo = new ToDo(
			        		 addNameField.getText(),
			        		 addSescriptionArea.getText(),
			        		 new Date((int)addDueDateYearSpinner.getValue()-1900, (int)addDueDateMonthSpinner.getValue()-1, (int)addDueDateDaySpinner.getValue()),
			        		 (Priority) addPriorityComboBox.getSelectedItem(),
			        		 addIsDoneCheckBox.isSelected()
			        		 );
			         
			         ((ToDoListModel) list.getModel()).add(addedToDo);
			         
			      }
				
			}
		});
		
		exit.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
				
			}
		});
		
		fileMenu.add(newTodo);
		fileMenu.add(exit);				
		
		//Help menu
		JMenuItem about = new JMenuItem("About");
		
		about.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(ToDoAppFrame.this,
					    "ToDo Application",
					    "About",
					    JOptionPane.PLAIN_MESSAGE);
				
			}
		});
		
		helpMenu.add(about);
		
		//add menus
		menuBar.add(fileMenu);		
		menuBar.add(helpMenu);
		
		setJMenuBar(menuBar);
		
		
	}
	
	
	private void createToDoData() {

		toDoList = new ArrayList<ToDo>();
		
	
		 try
	        {
	            FileInputStream fileInputStream = new FileInputStream("tododata");
	            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
	            toDoList = (ArrayList) objectInputStream.readObject();
	            objectInputStream.close();
	            fileInputStream.close();
	         }catch(IOException ioe){
	             ioe.printStackTrace();
	             return;
	          }catch(ClassNotFoundException c){
	             System.out.println("Class not found");
	             c.printStackTrace();
	             return;
	          }
		
	}
	
	private void saveToDoData() {
		
		try{
	         FileOutputStream fileOutputStream= new FileOutputStream("tododata");
	         ObjectOutputStream objectOutputStream= new ObjectOutputStream(fileOutputStream);
	         objectOutputStream.writeObject(toDoList);
	         objectOutputStream.close();
	         fileOutputStream.close();
	       }catch(IOException ioe){
	            ioe.printStackTrace();
	        }
		
	}

	
	
	private void createContent() {	
		
		// side bar
		
		filterComboBox = new JComboBox<Filter>();
		filterComboBox.addItem(Filter.DONE);
		filterComboBox.addItem(Filter.EXPIRED);
		filterComboBox.addItem(Filter.UNDONE_LOW_PRIORITY);
		filterComboBox.addItem(Filter.UNDONE_MEDIUM_PRIORITY);
		filterComboBox.addItem(Filter.UNDONE_HIGH_PRIORITY);
		
		filterValue = Filter.DONE;
		
		filterComboBox.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				filterValue = (Filter)filterComboBox.getSelectedItem();				
				model.update(filterValue);
				
			}
		});
		
		add(filterComboBox, BorderLayout.NORTH);
				
		model = new ToDoListModel(filterValue, toDoList);
		list = new JList(model);
		
		ListCellRenderer cellRenderer = new ToDoListCellRenderer();
		list.setCellRenderer(cellRenderer);
		
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setLayoutOrientation(JList.VERTICAL);
		list.setVisibleRowCount(-1);
		list.setFixedCellWidth(200);
		list.setFixedCellHeight(40);
		
		list.addMouseListener(new MouseAdapter() {
		    public void mouseClicked(MouseEvent evt) {
		    JList list = (JList)evt.getSource();
		    int index = list.locationToIndex(evt.getPoint());
		    
		    ToDo selectedToDo = ((ToDo) list.getModel().getElementAt(index));
		    System.out.println(((ToDo) list.getModel().getElementAt(index)).getName());
		    
		    nameField.setText(selectedToDo.getName());
			descriptionArea.setText(selectedToDo.getDescription());
			dueDateYearSpinner.setValue(selectedToDo.getDueDate().getYear()+1900);
			dueDateMonthSpinner.setValue(selectedToDo.getDueDate().getMonth()+1);
			dueDateDaySpinner.setValue(selectedToDo.getDueDate().getDate());
			priorityComboBox.setSelectedItem(selectedToDo.getPriority());
			isDoneCheckBox.setSelected(selectedToDo.isDone());
			
			ToDoIndex = index;
		    }
		});
		
		
		JScrollPane scrollPane = new JScrollPane(list);
		scrollPane.setMinimumSize(new Dimension(400, 800));
		
		add(scrollPane, BorderLayout.LINE_START);
		
		
		// description panel defs
		
		JPanel descriptionPanel = new JPanel();
		descriptionPanel.setBorder(new TitledBorder("ToDo"));
		
		JLabel nameLabel = new JLabel("Name:");
		JLabel descriptionLabel = new JLabel("Description:");
		JLabel dueDateLabel = new JLabel("Due Date:");
		JLabel priorityLabel = new JLabel("Priority:");
		
		nameField = new JTextField(30);
		descriptionArea = new JTextArea(5, 80);
		
		dueDateYearSpinner = new JSpinner();
		dueDateMonthSpinner = new JSpinner();		
		dueDateDaySpinner = new JSpinner();
		
		priorityComboBox = new JComboBox<Priority>();
		
		isDoneCheckBox = new JCheckBox("Task completed");
		
		JButton editButton = new JButton("Edit");
		JButton deleteButton = new JButton("Delete");
		JButton completeButton = new JButton("Task completed");
		JButton saveChangesButton = new JButton("Save changes");
		
		// description panel conf
		
		nameField.setEditable(false);
		descriptionArea.setEditable(false);
		dueDateYearSpinner.setEnabled(false);
		dueDateMonthSpinner.setEnabled(false);
		dueDateDaySpinner.setEnabled(false);
		priorityComboBox.setEnabled(false);
		isDoneCheckBox.setEnabled(false);
		
		Date currentDate = new Date();
		SpinnerModel yearModel = new SpinnerNumberModel(currentDate.getYear()+1900, currentDate.getYear() +1800, currentDate.getYear() + 2000, 1);
		dueDateYearSpinner.setModel(yearModel);
		SpinnerModel monthModel = new SpinnerNumberModel(currentDate.getMonth()+1, 1, 12, 1);
		dueDateMonthSpinner.setModel(monthModel);
		SpinnerModel dayModel = new SpinnerNumberModel(currentDate.getDate(), 1, 31, 1);
		dueDateDaySpinner.setModel(dayModel);
		
		priorityComboBox.addItem(Priority.LOW_PRIORITY);
		priorityComboBox.addItem(Priority.MEDIUM_PRIORITY);
		priorityComboBox.addItem(Priority.HIGH_PRIORITY);
		
		saveChangesButton.setVisible(false);
		
		completeButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (ToDoIndex!=-1){
				((ToDo) list.getModel().getElementAt(ToDoIndex)).setDone(true);
				
				ToDoIndex=-1;
				nameField.setText("");
				descriptionArea.setText("");
				dueDateYearSpinner.setValue(new Date().getYear() +1900);
				dueDateMonthSpinner.setValue(new Date().getMonth() +1);
				dueDateDaySpinner.setValue(new Date().getDate());
				priorityComboBox.setSelectedItem(Priority.LOW_PRIORITY);
				isDoneCheckBox.setSelected(false);
				}
				
				
			}
		});
		
		editButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (ToDoIndex!=-1){
				nameField.setEditable(true);
				descriptionArea.setEditable(true);
				dueDateYearSpinner.setEnabled(true);
				dueDateMonthSpinner.setEnabled(true);
				dueDateDaySpinner.setEnabled(true);
				priorityComboBox.setEnabled(true);
				isDoneCheckBox.setEnabled(true); 
				
				editButton.setVisible(false);
				saveChangesButton.setVisible(true);
				}
			}
		});
		
		saveChangesButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (ToDoIndex!=-1){
				nameField.setEditable(false);
				descriptionArea.setEditable(false);
				dueDateYearSpinner.setEnabled(false);
				dueDateMonthSpinner.setEnabled(false);
				dueDateDaySpinner.setEnabled(false);
				priorityComboBox.setEnabled(false);
				isDoneCheckBox.setEnabled(false);
				
				editButton.setVisible(true);
				saveChangesButton.setVisible(false);
				
				((ToDo) list.getModel().getElementAt(ToDoIndex)).setName(nameField.getText());
				((ToDo) list.getModel().getElementAt(ToDoIndex)).setDescription(descriptionArea.getText());
				((ToDo) list.getModel().getElementAt(ToDoIndex)).setDueDate(new Date((int)dueDateYearSpinner.getValue()-1900, (int)dueDateMonthSpinner.getValue()-1, (int)dueDateDaySpinner.getValue()));
				((ToDo) list.getModel().getElementAt(ToDoIndex)).setPriority((Priority)priorityComboBox.getSelectedItem());
				((ToDo) list.getModel().getElementAt(ToDoIndex)).setDone(isDoneCheckBox.isSelected());
				
				ToDoIndex=-1;
				nameField.setText("");
				descriptionArea.setText("");
				dueDateYearSpinner.setValue(new Date().getYear() +1900);
				dueDateMonthSpinner.setValue(new Date().getMonth() +1);
				dueDateDaySpinner.setValue(new Date().getDate());
				priorityComboBox.setSelectedItem(Priority.LOW_PRIORITY);
				isDoneCheckBox.setSelected(false);
				}
				
			}
		});
		
		deleteButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (ToDoIndex!=-1){
					
					
					ToDo deletedToDo = ((ToDo) list.getModel().getElementAt(ToDoIndex));
					((ToDoListModel) list.getModel()).delete(deletedToDo);
					
					ToDoIndex=-1;
					nameField.setText("");
					descriptionArea.setText("");
					dueDateYearSpinner.setValue(new Date().getYear() +1900);
					dueDateMonthSpinner.setValue(new Date().getMonth() +1);
					dueDateDaySpinner.setValue(new Date().getDate());
					priorityComboBox.setSelectedItem(Priority.LOW_PRIORITY);
					isDoneCheckBox.setSelected(false);
				}
				
			}
		});
		
				
		// description add components
				
		JPanel namePanel = new JPanel();
		JPanel descriptionsPanel = new JPanel();
		JPanel dueDatePanel = new JPanel();
		JPanel priorityPanel = new JPanel();
		JPanel buttonsPanel = new JPanel();
		
		namePanel.add(nameLabel);
		namePanel.add(nameField);
		
		descriptionsPanel.add(descriptionLabel);
		descriptionsPanel.add(descriptionArea);
		
		dueDatePanel.add(dueDateLabel);
		dueDatePanel.add(dueDateYearSpinner);
		dueDatePanel.add(dueDateMonthSpinner);
		dueDatePanel.add(dueDateDaySpinner);
		
		priorityPanel.add(priorityLabel);
		priorityPanel.add(priorityComboBox);
		
		buttonsPanel.add(isDoneCheckBox);
		buttonsPanel.add(completeButton);
		buttonsPanel.add(deleteButton);
		buttonsPanel.add(editButton);
		buttonsPanel.add(saveChangesButton);
		
		descriptionPanel.add(namePanel);
		descriptionPanel.add(descriptionsPanel);
		descriptionPanel.add(dueDatePanel);
		descriptionPanel.add(priorityPanel);
		descriptionPanel.add(buttonsPanel);
		descriptionPanel.add(Box.createRigidArea(new Dimension(0,450)));
		
		namePanel.setAlignmentX(Component.LEFT_ALIGNMENT); 
		descriptionsPanel.setAlignmentX(Component.LEFT_ALIGNMENT); 
		dueDatePanel.setAlignmentX(Component.LEFT_ALIGNMENT); 
		priorityPanel.setAlignmentX(Component.LEFT_ALIGNMENT); 
		buttonsPanel.setAlignmentX(Component.LEFT_ALIGNMENT); 
		
		
		descriptionPanel.setLayout(new BoxLayout(descriptionPanel, BoxLayout.Y_AXIS));
		add(descriptionPanel, BorderLayout.CENTER);
		
	}

}
