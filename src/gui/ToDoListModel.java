package gui;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.ArrayList;

import javax.swing.AbstractListModel;

import data.Filter;
import data.Priority;
import data.ToDo;

public class ToDoListModel extends AbstractListModel {
	
	ArrayList<ToDo> toDoList;
	Filter filterValue;
	
	
	public ToDoListModel(Filter filterValue, ArrayList<ToDo> list) {
		this.filterValue = filterValue;		
		toDoList = list;		
	}

	@Override
	public Object getElementAt(int index) {
		
		ArrayList<ToDo> filteredToDoList = new ArrayList<ToDo>();
		
		if (filterValue == Filter.DONE) {
			for (ToDo toDo : toDoList) {
				if (toDo.isDone()) filteredToDoList.add(toDo);
			}
		}
		else if (filterValue == Filter.EXPIRED) {
			for (ToDo toDo : toDoList) {
				if (!toDo.isDone() && toDo.getDueDate().before(new Date())) filteredToDoList.add(toDo);
			}
		}
		else if (filterValue == Filter.UNDONE_LOW_PRIORITY) {
			for (ToDo toDo : toDoList) {
				if (!toDo.isDone() && toDo.getDueDate().after(new Date()) && toDo.getPriority() == Priority.LOW_PRIORITY ) filteredToDoList.add(toDo);
			}
		}
		else if (filterValue == Filter.UNDONE_MEDIUM_PRIORITY) {
			for (ToDo toDo : toDoList) {
				if (!toDo.isDone() && toDo.getDueDate().after(new Date()) && toDo.getPriority() == Priority.MEDIUM_PRIORITY ) filteredToDoList.add(toDo);
			}
		}
		else if (filterValue == Filter.UNDONE_HIGH_PRIORITY) {
			for (ToDo toDo : toDoList) {
				if (!toDo.isDone() && toDo.getDueDate().after(new Date()) && toDo.getPriority() == Priority.HIGH_PRIORITY ) filteredToDoList.add(toDo);
			}
		}
		
		Collections.sort(filteredToDoList, new Comparator<ToDo>() {

			@Override
			public int compare(ToDo o1, ToDo o2) {
				return o1.getDueDate().compareTo(o2.getDueDate());
			}
			
		});
		
		return filteredToDoList.get(index);
	}

	@Override
	public int getSize() {
		
		ArrayList<ToDo> filteredToDoList = new ArrayList<ToDo>();

		if (filterValue == Filter.DONE) {
			for (ToDo toDo : toDoList) {
				if (toDo.isDone()) filteredToDoList.add(toDo);
			}
		}
		else if (filterValue == Filter.EXPIRED) {
			for (ToDo toDo : toDoList) {
				if (!toDo.isDone() && toDo.getDueDate().before(new Date())) filteredToDoList.add(toDo);
			}
		}
		else if (filterValue == Filter.UNDONE_LOW_PRIORITY) {
			for (ToDo toDo : toDoList) {
				if (!toDo.isDone() && toDo.getDueDate().after(new Date()) && toDo.getPriority() == Priority.LOW_PRIORITY ) filteredToDoList.add(toDo);
			}
		}
		else if (filterValue == Filter.UNDONE_MEDIUM_PRIORITY) {
			for (ToDo toDo : toDoList) {
				if (!toDo.isDone() && toDo.getDueDate().after(new Date()) && toDo.getPriority() == Priority.MEDIUM_PRIORITY ) filteredToDoList.add(toDo);
			}
		}
		else if (filterValue == Filter.UNDONE_HIGH_PRIORITY) {
			for (ToDo toDo : toDoList) {
				if (!toDo.isDone() && toDo.getDueDate().after(new Date()) && toDo.getPriority() == Priority.HIGH_PRIORITY ) filteredToDoList.add(toDo);
			}
		}
		
		fireContentsChanged(this, 0, 0);
		
		return filteredToDoList.size();
	}
	
	public void update(Filter newFilterValue){
				
		this.filterValue = newFilterValue;
	}
	
	public void delete(ToDo toDo){
		
		toDoList.remove(toDo);
		fireContentsChanged(this, 0, 0);
	}
	
	public void add(ToDo toDo){
		
		toDoList.add(toDo);
		fireContentsChanged(this, 0, 0);
	}

}
